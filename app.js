require('babel-register');
//const {success, error, getIndex} = require('function');
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const config = require('./config.json')

app.use(morgan('dev'));

//permet de gerer les requetes Cross Origin
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//middleware permettant de debug les requete http pendant la phase de développement
app.use(morgan('dev'));
// permet de parser le json
app.use(bodyParser.json()); 
// pour parser l'application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 

//création d'un router
let ironMan = express.Router();

//on inclus nos fonction dans notre routeur
ironMan.route('/yo')
            //fonction qui nous permet de récuperer donné du cv par colonne
            .get((req, res)=>{
                res.status(200).json("Hey yo ! :)")
            })



// on précise sur quoi agit notre routeur MembersRouter
app.use(config.rootAPI+'ironman', ironMan);


//on donne un port a notre application avec une fonction de call back pour confirmer la conection
app.listen(config.port,()=> console.log(`Started on port ${config.port}`));


