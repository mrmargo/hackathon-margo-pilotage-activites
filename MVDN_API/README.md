# Mouvement du Nid
##### Défi de valorisation et de pilotage des activités grâce aux données terrain

À l'occasion du hackathon Tech for Good organisé par [Margo](http://margo-group.com/), [Latitudes](http://latitudes.cc/) et [makesense](https://makesense.org/), l'association [Mouvement du Nid](mouvementdunid.org) chercher à améliorer la valorisation et le pilotage de ses activités grâce aux données recueillies par ses bénévoles sur le terrain. 

##### #1 | Présentation de Mouvement du Nid
Le Mouvement du Nid est une association 1901 reconnue d'utilité publique qui rencontre et soutient des milliers de personnes prostituées (femmes, hommes, trans, victimes ou non de la traite des êtres humains) chaque année. L'association agit aussi pour la formation des professionnel.le.s du travail social, la prévention et le plaidoyer féministe et abolitionniste.
Les actions menées sont les suivantes : 
+ rencontre et accompagnement des personnes en situation de prostitution ; 
+ prévention du risque prostitutionnel ;
+ sensibilisation du public et formation des professionnel.le.s de l’action sociale.

##### #2 | Problématique : valorisation et pilotage des actions menées par les bénévoles
Environ 600 bénévoles agissent aux côtés des 17 salarié.e.s de l'association pour mener à bien la mission de Mouvement du Nid, et plus de 2000 actions sont menées chaque année pour toucher des milliers de bénéficiaires. 
Comme d’autres associations agissant en soutien à des publics particulièrement vulnérables, Mouvement du Nid travaille sans cesse à trouver des solutions, à mettre les personnes à l’abri. Peu de temps est consacré à consigner les actions réalisées, il n’y a pas de pause entre deux urgences.
> La problématique : comment assurer un suivi des actions menées par les bénévoles, afin de valoriser ces actions en interne et en externe et d'améliorer le pilotage des activités de Mouvement du Nid ? 

##### #3 | Le défi proposé
Sur la base d'un questionnaire (Google Form) intégré à l'intranet de l'association qui permet aux bénévoles de renseigner chaque action menée sur le terrain – et de récolter les données sur une spreadsheet – le défi est d'imaginer des services qui répondent aux besoins suivants : 
+ valoriser en interne les actions menées par les bénévoles et les salarié.e.s
+ valoriser en externe les actions menées par les bénévoles et les salarié.e.s
+ faciliter le pilotage des activités par l'équipe salariée de Mouvement du Nid grâce à des outils de visualisation de données, comme par un exemple un dashboard

##### #4 | Livrables
Les livrables attendus sont des services mis en production, et testés sur des données tests, qui répondent au défi. Les participant.e.s pourront s'appuyer sur les exemples donnés ci-dessous, et surtout être force de proposition pour imaginer des services qui répondent aux enjeux de Mouvement du Nid.

Exemples de services attendus :
+ Mailing automatisé qui envoie à l'ensemble de l'association un récapitulatif des actions menées le mois précédent
+ Dashboard de suivi des activités et ses analytics
 
##### #5 | Ressources à disposition pour résoudre le défi


###### Google form et spreadsheet associés à la remontée des données terrains
Au début du hackathon, chaque équipe recevra un lien vers le questionnaire, et un set de données fictives sur les activités de Mouvement du Nid. 
Ces éléments lui permettront de développer des services et de les tester sur ces données.

###### Mise en production des services
Le Mouvement du Nid dispose d'un serveur mutualisé sur phpnet.org à l'heure actuelle. Il permet la mise en place d'applications en PHP, en Python et en Perl.  
L'intranet est déployé directement chez Wizengo, le prestataire qui a mis en place l'intranet.  
Si d'autres technologies sont utilisées, il faudra donc prévoir un hébergement des services pour qu'ils puissent être exploités. Une solution peut être de s'orienter vers une plateforme comme [Heroku](https://www.heroku.com).  
Toutefois, d'autres solutions peuvent être proposées, et le choix final dépendra principalement des technologies utilisées pour les services.

###### Intégration à l'intranet
+ Les services destinés à afficher des informations sur une page de l'intranet devront présenter une API qui sera lue depuis l'intranet. Un swagger peut permettre de visualiser la sortie de l'API.
+ Les services destinés à créer des évènements dans le calendrier devront générer un flux respectant le standard iCalendar, que le plugin Pro Event Calendar de l'intranet pourra suivre.
+ Les services destinés à l'envoi de mail devront être pensés en deux étapes : un étape de notification en attente de validation, puis une étape d'envoi.
+ Il est également possible de passer via un [plugin Wordpress](https://wpformation.com/creer-plugin-wordpress/).

###### Instance de l'intranet
L'Intranet est basé sur WordPress, donc en PHP/MySQL, qui fonctionne essentiellement à base de plugins.  
Selon les besoins, il peut être possible de lui ajouter un plugin, mais il est également possible, depuis les pages de l'intranet, d'accéder à des API externes.  
Les plugins existants sont notamment :  
+ Pro Event Calendar pour la gestion du calendrier.  
+ bbPress pour la gestion du forum.  

L'url vers une instance dédiée de l'intranet sera fournie à chaque équipe le jour du hackaton :  
+ Equipe 1 : http://4d298f50.ngrok.io/
+ Equipe 2 : http://80e09c44.ngrok.io/
+ Equipe 3 : http://968f2567.ngrok.io/


##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Margo, Latitudes et makesense ont voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux associations, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux associations d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateurs, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les associations soutenues, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.


##### #7 | Points de contact lors du hackathon
Élise G. : salariée au Mouvement du Nid  
Sabrina P. : consultante Margo Consulting et en charge de la préparation du défi Mouvement du Nid  
Augustin C. : co-fondateur de Latitudes, et en charge de la préparation du défi Mouvement du Nid



