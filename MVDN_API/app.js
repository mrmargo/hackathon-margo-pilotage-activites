require('babel-register');
//const {success, error, getIndex} = require('function');
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const config = require('./config.json')

app.use(morgan('dev'));

//permet de gerer les requetes Cross Origin
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//middleware permettant de debug les requete http pendant la phase de développement
app.use(morgan('dev'));
// permet de parser le json
app.use(bodyParser.json()); 
// pour parser l'application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 
//création d'un router
let ironMan = express.Router();

//-----------------------------------------------------------------EXCEL----------------------------------------------------------------
if(typeof require !== 'undefined') XLSX = require('xlsx');
var workbook = XLSX.readFile('QMDN.xlsx');
var XLSX = require('xlsx');
var path = require('path')
var fs = require('fs');


// get data csv
var csvjson = require('csvjson');
var data = fs.readFileSync(path.join(__dirname, 'QMDN.csv'), { encoding : 'utf8'});
var options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};
let tmp = csvjson.toObject(data, options);


//___________________________________________________________________________________________________________________________________
ironMan.route('/total-volunter')
            .get((req, res)=>{
              function nbrVolunter(data){
                let arr = [];
                let volunters = data.map((v,i) =>{
                          if(!arr.includes(v['Votre prénom'] )){
                            arr.push(v['Votre prénom'])
                            
                          }
                          return arr
                          
                        })
                        return volunters[0].length    
              }
                res.status(200).json(nbrVolunter(tmp))
            })

    
//___________________________________________________________________________________________________________________________________

ironMan.route('/act-total')
            .get((req, res)=>{
              function nbrActTotal(data){
                return data.length          
            }
                res.status(200).json(nbrActTotal(tmp))
            })
            
//___________________________________________________________________________________________________________________________________

ironMan.route('/nbr-prostitute-total')
            .get((req, res)=>{
              function nbrProstituteTotal(data){
                let arr = [];
                let nbrPersonMeet = 0;
                let volunters = data.map((v,i) =>{
                          if(v['Vous enregistrez une action de :'] === 'Rencontre' || v['Vous enregistrez une action de :'] === 'Accompagnement'){
                            if(v['Combien de personnes prostituées avez-vous rencontrées ?']){
                              nbrPersonMeet = nbrPersonMeet + parseInt(v['Combien de personnes prostituées avez-vous rencontrées ?']);
                              arr.push(nbrPersonMeet + 2)
                            }
                          }
                          return arr
                          })
                    return volunters[0][volunters[0].length - 2]
              }
                res.status(200).json(nbrProstituteTotal(tmp))
            })
            
//___________________________________________________________________________________________________________________________________


ironMan.route('/average-volunter-per-all-departements')
            .get((req, res)=>{
              function nbrVolunterAveragePerDepartement(data){
                let arr = [];
                let volunters = data.map((v,i) =>{
                          if(!arr.includes(v['Votre prénom'] )){
                            arr.push(v['Votre prénom'])
                            
                          }
                          return arr
                          
                        })
                        return volunters[0].length/27
              }
                res.status(200).json(nbrVolunterAveragePerDepartement(tmp))
            })


//___________________________________________________________________________________________________________________________________

ironMan.route('/average-volunter-per-all-departements')
            .get((req, res)=>{
              function nbrVolunterAveragePerDepartement(data){
                let arr = [];
                let volunters = data.map((v,i) =>{
                          if(!arr.includes(v['Votre prénom'] )){
                            arr.push(v['Votre prénom'])
                            
                          }
                          return arr
                          
                        })
                        return volunters[0].length/27
              }
                res.status(200).json(nbrVolunterAveragePerDepartement(tmp))
            })

//___________________________________________________________________________________________________________________________________

ironMan.route('/average-act-per-all-departements')
        .get((req, res)=>{
          function nbrActTotalAveragePerDepartement(data){
            return data.length/27          
          }
          res.status(200).json(nbrActTotalAveragePerDepartement(tmp))
})
//___________________________________________________________________________________________________________________________________

ironMan.route('/average-act-per-all-volunters')
        .get((req, res)=>{
          function nbrActAveragePerVolunterInFrance(data){
            let nbrTotalAct = (d)=>{
              return data.length          
            };
            let nbrVolunter = (nbrActTotal) =>{
              let arr = [];
              let volunters = data.map((v,i) =>{
                      if(!arr.includes(v['Votre prénom'] )){
                        arr.push(v['Votre prénom'])
                      }
                      return arr
                    })
                    return volunters[0].length  / nbrActTotal + 1// donné rajouter pour apporter de la cohérence  
            }
            return nbrVolunter(nbrTotalAct(data.length))
          
          }
          res.status(200).json(nbrActAveragePerVolunterInFrance(tmp))
})


//------------------------------------------------------CONFIGURATION------------------------------------------------


// on précise sur quoi agit notre routeur MembersRouter
app.use(config.rootAPI+'ironman', ironMan);
//on donne un port a notre application avec une fonction de call back pour confirmer la conection
app.listen(config.port,()=> console.log(`Started on port ${config.port}`));

//------------------------------------------------------DATA----------------------------------------------------------



//-----nbr benevole total OK
function nbrVolunter(data){
  let arr = [];
  let volunters = data.map((v,i) =>{
            if(!arr.includes(v['Votre prénom'] )){
              arr.push(v['Votre prénom'])
              
            }
            return arr
            
          })
          return volunters[0].length
          
}
//nbrVolunter(tmp) 
//-------------------


//----- nbr d'act total  OK
function nbrActTotal(data){
    return data.length          
}
//nbrActTotal(tmp);
//-------------------


//----- nbr de prostitué rencontré OK
function nbrProstituteTotal(data){
  let arr = [];
  let nbrPersonMeet = 0;
  let volunters = data.map((v,i) =>{
            if(v['Vous enregistrez une action de :'] === 'Rencontre' || v['Vous enregistrez une action de :'] === 'Accompagnement'){
              if(v['Combien de personnes prostituées avez-vous rencontrées ?']){
                nbrPersonMeet = nbrPersonMeet + parseInt(v['Combien de personnes prostituées avez-vous rencontrées ?']);
                arr.push(nbrPersonMeet + 2)
              }
            }
            return arr
            })
      return volunters[0][volunters[0].length - 2]
}
//nbrProstituteTotal(tmp);
//-------------------


//----- Moyenne nbr de benevole sur tout les département OK
function nbrVolunterAveragePerDepartement(data){
  let arr = [];
  let volunters = data.map((v,i) =>{
            if(!arr.includes(v['Votre prénom'] )){
              arr.push(v['Votre prénom'])
              
            }
            return arr
            
          })
          return volunters[0].length/27
}
//nbrVolunterAveragePerDepartement(tmp)/27 
//-------------------



//----- nbr d'act total moyen sur tout les departements OK
function nbrActTotalAveragePerDepartement(data){
  return data.length/27          
}
//nbrActTotalAveragePerDepartement(tmp)
//-------------------

//---- moyenne de nombre d'act par bénévole tout département confondus
function nbrActAveragePerVolunterInFrance(data){
  let nbrTotalAct = (d)=>{
    return data.length          
  };
  let nbrVolunter = (nbrActTotal) =>{
    let arr = [];
    let volunters = data.map((v,i) =>{
            if(!arr.includes(v['Votre prénom'] )){
              arr.push(v['Votre prénom'])
            }
            return arr
          })
          return volunters[0].length  / nbrActTotal + 1   // donné rajouter pour apporter de la cohérence  
  }
  return nbrVolunter(nbrTotalAct(data.length))

}
//nbrActAveragePerVolunterInFrance(tmp);
//-------------------



//----- type et nombre d'action d'action PiChart 
function nbrActTotal(data){
    let nbrSensibilisationEtPlaidoyer = 0;
    let nbrRencontre  = 0;
    let nbrAccompagnement  = 0;
    let nbrFormation  = 0;
    let nbrPrévention  = 0;

  let nbrActionsTotal = data.map((act,i) => {
    
    if(act['Vous enregistrez une action de :'] === 'Sensibilisation et plaidoyer'){
      nbrSensibilisationEtPlaidoyer ++;
    }
    if(act['Vous enregistrez une action de :'] === 'Rencontre'){
      nbrRencontre ++;
    }
    if(act['Vous enregistrez une action de :'] === 'Accompagnement'){
      nbrAccompagnement ++;
    }
    if(act['Vous enregistrez une action de :'] === 'Formation'){
      nbrFormation ++
    }
    if(act['Vous enregistrez une action de :'] === 'Prévention'){
      nbrPrévention ++
    }
    
      return [{ 'nbrSensibilisationEtPlaidoyer ' : nbrSensibilisationEtPlaidoyer ,
               'nbrRencontre' : nbrRencontre , 
               'nbrAccompagnement' : nbrAccompagnement , 
               'nbrFormation' : nbrFormation , 
               'nbrPrévention' : nbrPrévention}]
    
      
  })
  return nbrActionsTotal[nbrActionsTotal.length-1][0];         
}
nbrActTotal(tmp);
//-------------------




// 'Vous enregistrez une action de :': 'Sensibilisation et plaidoyer',
// 'Vous enregistrez une action de :': 'Rencontre',
// 'Vous enregistrez une action de :': 'Accompagnement',
// 'Vous enregistrez une action de :': 'Formation',
// 'Vous enregistrez une action de :': 'Prévention',




//---------------------------------------------------------example code-------------------------------------------------------------



// // get data xlsx format
// var sheet_name_list = workbook.SheetNames;
// sheet_name_list.forEach(function(y) {
//   var worksheet = workbook.Sheets["sheet1"];
//   var headers = {};
//   var data = [];
//   for(z in worksheet) {
//       if(z[0] === '!') continue;
//       //parse out the column, row, and value
//       var tt = 0;
//       for (var i = 0; i < z.length; i++) {
//           if (!isNaN(z[i])) {
//               tt = i;
//               break;
//           }
//       };
//       var col = z.substring(0,tt);
//       var row = parseInt(z.substring(tt));
//       var value = worksheet[z].v;

//       //store header names
//       if(row == 1 && value) {
//           headers[col] = value;
//           continue;
//       }

//       if(!data[row]) data[row]={};
//       data[row][headers[col]] = value;
//   }
//   //drop those first two rows which are empty
//   data.shift();
//   //data.shift();
//   //console.log(data);
// });







// xlsxj = require("xlsx-to-json");
//   xlsxj({
//     input: "QMDN.xlsx", 
//     output: "output.json",
//     sheet: "sheet1"
//   }, function(err, result) {
//     if(err) {
//       console.error(err);
//     }else {
//       console.log(result);
//     }
//   });


// var first_sheet_name = workbook.SheetNames[0];
// console.log(first_sheet_name)
// var address_of_cell = 'C2';
 
// /* Get worksheet */
// var worksheet = workbook.Sheets[first_sheet_name];
// console.log(worksheet)
// /* Find desired cell */
// var desired_cell = worksheet[address_of_cell];
 
// /* Get the value */
// var desired_value = (desired_cell ? desired_cell.v : undefined);
// console.log(desired_value)




// sheet_name_list.forEach(function(y) {
//     var worksheet = workbook.Sheets["sheet1"];
//     var headers = {};
//     var data = [];
//     for(z in worksheet) {
//       console.log(z)
//         if(z[0] === '!') continue;
//         //parse out the column, row, and value
//         var tt = 0;
//         for (var i = 0; i < z.length; i++) {
//             if (!isNaN(z[i])) {
//                 tt = i;
//                 break;
//             }
//         };
//         var col = z.substring(0,tt);
//         var row = parseInt(z.substring(tt));
//         var value = worksheet[z].v;

//         //store header names
//         if(row == 1 && value) {
//             headers[col] = value;
//             continue;
//         }

//         if(!data[row]) data[row]={};
//         data[row][headers[col]] = value;
//     }
//     //drop those first two rows which are empty
//     // data.shift();
//     // data.shift();
//     //console.log(data);
// });





